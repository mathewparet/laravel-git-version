# mathewparet/laravel-git-version

Get version number from git or fallback

# Installation

```sh
composer require mathewparet/laravel-git-version
sail artisan vendor:publish --tag=laravel-git-version
```

# Usage

To get the application version:

```php
// config/git.php
use mathewparet\LaravelGitVersion\Git\GitVersion;

/**
 * Example #1 - get version from git,
 * or fallback and read the contents of the 
 * `.current_version` file in the application 
 * root.
 */
'version' => GitVersion::version();


/**
 * Example #2 - get version from git,
 * or fallback and run the closure.
 */
'version' => GitVersion::version(function() {
    return env('APP_VERSION');
});
```
