<?php
namespace mathewparet\LaravelGitVersion\Git;

use Carbon\Carbon;
use mathewparet\LaravelGitVersion\Contracts\GitVersion as ContractsGitVersion;

class GitVersion implements ContractsGitVersion
{
    public static function version($closure = null)
    {
        return self::isExecFunctionEnabled()
                    ? self::deriveVersionNumberFromGit()
                    : ($closure ? $closure() : self::fallBackVersion());
    }

    public static function date($closure = null)
    {
        return self::isExecFunctionEnabled()
                    ? self::deriveVersionDateFromGit()
                    : ($closure ? $closure() : null);
    }

    public static function branch($closure = null)
    {
        return self::isExecFunctionEnabled()
                    ? self::deriveBranchNameFromGit()
                    : ($closure ? $closure() : null);
    }

    private static function isExecFunctionEnabled()
    {
        return !in_array('exec', explode(",", ini_get('disable_functions')));
    }

    private static function deriveVersionNumberFromGit()
    {
        return exec("git describe");
    }
    
    private static function deriveBranchNameFromGit()
    {
        return exec("git branch | grep '*' | cut -c3-");
    }

    private static function deriveVersionDateFromGit()
    {
        return (new Carbon(exec('git log -n1 --pretty=%ci HEAD')))->tz('UTC');
    }

    private function fallBackVersion()
    {
        return trim(base_path('.current_version'));
    }
}