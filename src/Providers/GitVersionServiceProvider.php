<?php
namespace mathewparet\LaravelGitVersion\Providers;

use Illuminate\Support\ServiceProvider;

class GitVersionServiceProvider extends ServiceProvider
{
    const CONFIG_FILE = __DIR__.'/../config/git.php';
    
    public function register()
    {
        $this->mergeConfigFrom(self::CONFIG_FILE, 'git');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->definePublishableComponents();
    }

    private function definePublishableComponents()
    {
        $this->publishes([
            self::CONFIG_FILE => config_path('git.php')
        ], 'laravel-git-version');
    }
}