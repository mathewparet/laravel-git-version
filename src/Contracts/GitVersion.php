<?php
namespace mathewparet\LaravelGitVersion\Contracts;

interface GitVersion
{
    /**
     * Get version number from git or fallback
     * 
     * If `exec` isn't enabled in PHP then it falls back to `$closure`.
     * If `$closure` is `null` it falls back and returns contents of `.current_version` file in the root of the application.
     * 
     * @param \Closure|null $closure
     * @return string
     */
    public static function version($closure = null);
}